packages <- list('here', 
                 'config', 
                 'xlsx', 
                 'readxl', 
                 'dplyr',
                 'reshape2',
                 'tidyr',
                 'stringr',
                 'ggplot2',
                 'pheatmap',
                 'ComplexHeatmap',
                 'limma',
                 'edgeR',
                 'qvalue',
                 'ermineR',
                 'lme4')

for (package in packages){
  install.packages(package)
}