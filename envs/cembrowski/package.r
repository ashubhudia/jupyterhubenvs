packages <- list('Seurat', 'reshape2', 'ggplot2', 'dplyr', 'Matrix')

for (package in packages){
  install.packages(package)
}
